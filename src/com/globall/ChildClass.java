package com.globall;

public class ChildClass extends PaternClass {
	public void properties3() {
		System.out.println("properties3 in child class");

	}

	public static void main(String[] args) {
		Grantparent g = new Grantparent();// grant parent
		g.properite1();

		ParentClass p = new ParentClass();// parent
		p.properties2();

		ChildClass c = new ChildClass();// child
		c.properties3();

	}

}
