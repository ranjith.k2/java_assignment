package com.globall;

import java.util.ArrayList;
import java.util.List;

public class List_topic {
	public static void main(String[] args) {
		List<String> list = new ArrayList<>();
		list.add("a");
		list.add("b");
		list.add("c");
		list.add("d");
		list.add("e");
		list.add("f");
		System.out.println(list);

		list.set(2, "w");
		System.out.println(list);

		list.remove(3);
		System.out.println(list);
		boolean contains = list.contains("a");
		System.out.println(contains);
	}

}
